/**
 * A simple distributed app for testing GitLab/Kubernetes continous deployment:
 * - multiple load balanced pods serving http
 * - front end displaying Pod's local cluster ip
 * - session persistence on a separate Redis pod
 * - value increment and persistence for each user session
 */
'use strict';
const Koa = require('koa');
const app = new Koa();
const ip = require('ip');
const port = 8080;

// configure main logger object, default logging level set to info
const winston = require('winston');
const logger = winston.createLogger({
  level: process.env.LOG_LEVEL || 'info', // available env values: error, warn, info, debug
  transports: [
    new winston.transports.Console({
      stderrLevels: ['error', 'warn']
    }),
  ]
});

app.use(async (ctx,next) => {
  if (ctx.path === '/favicon.ico') return;
  await next();
});

// configure session cookie for 1 day with auto renewal
const session = require('koa-session');
const redisStore = require('koa-redis');
app.keys = ['never ever forever']
app.use(session({
  key: 'increment',
  maxAge: 1000*60*60*24*1,
  signed: true,
  renew: true,
  store: redisStore({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
  }),
}, app));

// increment and render the interface
const controller = require('./lib/controller.js');
const pug = require('pug');
app.use(controller(logger));
app.use(async (ctx) => {
  ctx.body = pug.renderFile('lib/template.pug', {
    session: ctx.session._sessCtx.externalKey,
    increment: ctx.session.increment,
    ip: ip.address(),
  });
});

app.listen(port);
logger.info(`listening on port ${port}`);
