module.exports = function controller(logger) {
	return async (ctx, next) => {
		if (!ctx.session.increment) {
			ctx.session.increment = 0;
		}
		let increment_current = ctx.session.increment;
		let increment_new = increment_current;
		if (ctx.query.increment !== undefined) {
			increment_new = increment_current + 1;
		}
		else if (ctx.query.reset !== undefined) {
			increment_new = 0;
		}
		if (increment_current != increment_new) {
			logger.debug(`changing increment from ${increment_current} to ${increment_new}`);
			ctx.session.increment = increment_new;
		}
		if (next !== undefined) {
			await next();
		}
	}
}
