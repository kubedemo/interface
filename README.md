## Node.js distributed app for testing GitLab/Kubernetes continous deployment

- Built, unit tested and deployed on GKE cluster on every commit
- Custom branches deployed to staging environment: https://staging.kubedemo.in
- Master branch deployed to production environment: https://kubedemo.in
- App served from multiple node.js pods (round-robin)
- Front-end shows a persisted session value on a separate Redis pod
- GKE clsuter configuerd as code from a separate GitLab repository: https://gitlab.com/kubedemo/cluster