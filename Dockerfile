FROM node:10.16-alpine
ENV NODE_ENV production
WORKDIR /opt/app
COPY package*.json ./
RUN npm install
EXPOSE 8080
COPY . .
CMD npm start