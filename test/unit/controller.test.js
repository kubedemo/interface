const controller = require('../../lib/controller.js');

let logger = {
  debug: jest.fn()
}
let ctx = {
  session: {
    increment: 0,
  },
  query: {
    increment: undefined,
    reset: undefined,
  }
}
let controller_middleware = controller(logger);
let next = jest.fn();

test('controller increments session correctly', () => {

  // increment value
  ctx.query = { increment: true };
  controller_middleware(ctx);
  expect(ctx.session.increment).toBe(1);

  // reset value
  ctx.query = { reset: true };
  controller_middleware(ctx);
  expect(ctx.session.increment).toBe(0);

  // debug was called
  expect(logger.debug.mock.calls.length).toBe(2);
});

test('controller calls middleware chain', () => {
  controller_middleware(ctx, next);
  expect(next.mock.calls.length).toBe(1);
});
